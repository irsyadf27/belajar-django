from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Matakuliah(models.Model):
    nama = models.CharField(max_length=50)
    kode = models.CharField(max_length=10)
    sks = models.PositiveIntegerField()
    semester = models.PositiveIntegerField()

    def __unicode__(self):
        return self.nama