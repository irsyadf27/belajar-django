from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings

from .models import Matakuliah
from belajar.apps.matakuliah.forms import FormMatakuliah

# Create your views here.
@login_required(login_url='/akun/login/')
def index(request):
    matakuliah = Matakuliah.objects.all().order_by('semester').order_by('nama')
    paginator = Paginator(matakuliah, settings.LIMIT_PAGE)
    page = request.GET.get('page')
    
    try:
        matakuliah = paginator.page(page)
    except PageNotAnInteger:
        matakuliah = paginator.page(1)
    except EmptyPage:
        matakuliah = paginator.page(paginator.num_pages)
        
    context = {'matakuliah': matakuliah}
    return render(request, 'matakuliah/index.html', context)

@login_required(login_url='/akun/login/')
def lihat(request, id):
    pass

@login_required(login_url='/akun/login/')
def tambah(request):
    form = FormMatakuliah(request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()

            messages.success(request, "Berhasil menambah matakuliah baru.")
            return redirect(reverse('matakuliah'))
    context = {
        'form': form
    }
    return render(request, "matakuliah/tambah.html", context)

@login_required(login_url='/akun/login/')
def edit(request, id):
    matakuliah = Matakuliah.objects.get(pk=id)
    form = FormMatakuliah(request.POST or None, instance=matakuliah)
    if request.POST:    
        if form.is_valid():
            form.save()
            
            messages.success(request, "Berhasil mengubah matakuliah.")
            return redirect(reverse('matakuliah'))
    else:
        context = {
            'id': id,
            'form': form
        }
        return render(request, "matakuliah/edit.html", context)

@login_required(login_url='/akun/login/')
def delete(request, id):
    matakuliah = Matakuliah.objects.get(pk=id)
    if matakuliah.delete():
        messages.success(request, "Berhasil menghapus matakuliah.")
    else:
        messages.danger(request, "Gagal menghapus matakuliah")
    return redirect(reverse('matakuliah'))