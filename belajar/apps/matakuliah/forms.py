from django import forms
from belajar.apps.matakuliah.models import Matakuliah

class FormMatakuliah(forms.ModelForm):
    class Meta:
        model = Matakuliah
        fields = ('nama', 'kode', 'sks', 'semester', )