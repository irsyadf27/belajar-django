from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='matakuliah'),
    url(r'^lihat/(?P<id>\d+)', views.lihat, name='lihatmatakuliah'),
    url(r'^tambah/$', views.tambah, name='tambahmatakuliah'),
    url(r'^edit/(?P<id>\d+)', views.edit, name='editmatakuliah'),
    url(r'^delete/(?P<id>\d+)', views.delete, name='deletematakuliah'),
]