from __future__ import unicode_literals

from django.db import models
from belajar.apps.mahasiswa.models import Mahasiswa
from belajar.apps.matakuliah.models import Matakuliah
from belajar.apps.bobotnilai.models import Bobotnilai
# Create your models here.
class Nilai(models.Model):
    mahasiswa = models.ForeignKey(Mahasiswa, on_delete=models.CASCADE)
    matakuliah = models.ForeignKey(Matakuliah, on_delete=models.CASCADE)
    nilai = models.ForeignKey(Bobotnilai, on_delete=models.CASCADE)

    def __unicode__(self):
        return "%s-%s-%d-%s" % (self.matakuliah.kode, self.matakuliah.nama, self.mahasiswa.nim, self.mahasiswa.nama)