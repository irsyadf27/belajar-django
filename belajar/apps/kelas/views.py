from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings

from .models import Kelas
from belajar.apps.mahasiswa.models import Mahasiswa
from belajar.apps.kelas.forms import KelasForm

# Create your views here.
@login_required(login_url='/akun/login/')
def index(request):
    list_kelas = Kelas.objects.all().order_by('nama')
    paginator = Paginator(list_kelas, settings.LIMIT_PAGE)
    page = request.GET.get('page')
    
    try:
        list_kelas = paginator.page(page)
    except PageNotAnInteger:
        list_kelas = paginator.page(1)
    except EmptyPage:
        list_kelas = paginator.page(paginator.num_pages)
        
    context = {'kelas': list_kelas}
    return render(request, 'kelas/index.html', context)

@login_required(login_url='/akun/login/')
def tambah(request):
    form = KelasForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()

            messages.success(request, "Berhasil menambah kelas baru.")
            return redirect(reverse('kelas'))
    context = {
        'form': form
    }
    return render(request, "kelas/tambah.html", context)

@login_required(login_url='/akun/login/')
def edit(request, id):
    kelas = Kelas.objects.get(pk=id)
    form = KelasForm(request.POST or None, instance=kelas)
    if request.POST:    
        if form.is_valid():
            form.save()
            
            messages.success(request, "Berhasil mengubah kelas.")
            return redirect(reverse('kelas'))
    else:
        context = {
            'id': id,
            'form': form
        }
        return render(request, "kelas/edit.html", context)
    
@login_required(login_url='/akun/login/')    
def delete(request, id):
    kelas = Kelas.objects.get(pk=id)
    if kelas.delete():
        messages.success(request, "Berhasil menghapus kelas.")
    else:
        messages.danger(request, "Gagal menghapus kelas")
    return redirect(reverse('kelas'))

@login_required(login_url='/akun/login/')
def lihat(request, id):
    data = Kelas.objects.get(pk=id)
    mahasiswa = Mahasiswa.objects.filter(kelas__id=id).order_by('nim')
    
    paginator = Paginator(mahasiswa, settings.LIMIT_PAGE)
    page = request.GET.get('page')
    
    try:
        mahasiswa = paginator.page(page)
    except PageNotAnInteger:
        mahasiswa = paginator.page(1)
    except EmptyPage:
        mahasiswa = paginator.page(paginator.num_pages)
        
    context = {
        'kelas': data,
        'mahasiswa': mahasiswa
    }
    return render(request, 'kelas/lihat.html', context)