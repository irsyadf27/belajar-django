from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='kelas'),
    url(r'^lihat/(?P<id>\d+)', views.lihat, name='lihatkelas'),
    url(r'^tambah/$', views.tambah, name='tambahkelas'),
    url(r'^edit/(?P<id>\d+)', views.edit, name='editkelas'),
    url(r'^delete/(?P<id>\d+)', views.delete, name='deletekelas'),
]