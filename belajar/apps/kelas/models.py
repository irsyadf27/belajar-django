from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Kelas(models.Model):
    nama = models.CharField(max_length=15)
    
    def __unicode__(self):
        return self.nama