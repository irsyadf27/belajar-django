from django import forms
from belajar.apps.kelas.models import Kelas

class KelasForm(forms.ModelForm):
    nama = forms.CharField(label='Nama Kelas', max_length=50, widget=forms.TextInput(attrs={'class': 'form-group'}))

    class Meta:
        model = Kelas
        fields = ('nama', )