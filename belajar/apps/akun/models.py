from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Profile(models.Model):
    akun = models.ForeignKey(User, on_delete=models.CASCADE)
    jk = models.CharField(max_length=1, choices=(('L', 'Laki-Laki'), ('P', 'Perempuan')))
    tgl_lahir = models.DateField()
    alamat = models.TextField()
    foto = models.ImageField(upload_to='foto')
    jenis = models.CharField(max_length=1, choices=(('A', 'Admin'), ('M', 'Mahasiswa')))
    
    def __unicode__(self):
        return "%s %s" % (self.akun.first_name, self.akun.last_name)