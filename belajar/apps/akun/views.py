from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout as default_logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse

from .models import Profile
# Create your views here.

def login(request):
    if request.POST:
        uname = request.POST['username']
        passw = request.POST['password']
        
        user = authenticate(username=uname, password=passw)
        if user is not None:
            if user.is_active:
                try:
                    akun = Profile.objects.get(akun=user.id)
                    login(request, user)
                    
                    request.session['user_id'] = user.id
                    request.session['jenis'] = akun.jenis
                except:
                    messages.add_message(request, messages.INFO, 'Anda Gagal login.')
                
                return redirect(reverse('home'))
                
            else:
                messages.add_message(request, messages.INFO, 'Akun anda belum aktif, silahkan hubungi administrator')
        else:
            messages.add_message(request, messages.INFO, 'Username atau password anda salah')
    
    return render(request, 'akun/login.html')

@login_required(login_url='/akun/login/')
def logout(request):
    default_logout(request)
    return redirect(reverse('login'))