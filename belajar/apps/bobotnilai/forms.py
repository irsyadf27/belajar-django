from django import forms
from belajar.apps.bobotnilai.models import Bobotnilai

class FormBobotnilai(forms.ModelForm):

    class Meta:
        model = Bobotnilai
        fields = ('indeks', 'bobot', )