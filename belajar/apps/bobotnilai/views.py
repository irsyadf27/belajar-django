from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings

from .models import Bobotnilai
from belajar.apps.bobotnilai.forms import FormBobotnilai

# Create your views here.
@login_required(login_url='/akun/login/')
def index(request):
    nilai = Bobotnilai.objects.all().order_by('-bobot')
    paginator = Paginator(nilai, settings.LIMIT_PAGE)
    page = request.GET.get('page')
    
    try:
        nilai = paginator.page(page)
    except PageNotAnInteger:
        nilai = paginator.page(1)
    except EmptyPage:
        nilai = paginator.page(paginator.num_pages)
        
    context = {'nilai': nilai}
    return render(request, 'bobotnilai/index.html', context)

@login_required(login_url='/akun/login/')
def tambah(request):
    form = FormBobotnilai(request.POST or None)
    if request.POST:
        if form.is_valid():
            #insert = Kelas(nama=form.cleaned_data.get('nama'))
            form.save()

            messages.success(request, "Berhasil menambah bobot nilai baru.")
            return redirect(reverse('bobotnilai'))
    context = {
        'form': form
    }
    return render(request, "bobotnilai/tambah.html", context)

@login_required(login_url='/akun/login/')
def edit(request, id):
    bobotnilai = Bobotnilai.objects.get(pk=id)
    form = FormBobotnilai(request.POST or None, instance=bobotnilai)
    if request.POST: 
        if form.is_valid():
            form.save()
            
            messages.success(request, "Berhasil mengubah bobot nilai.")
            return redirect(reverse('bobotnilai'))
    else:
        context = {
            'id': id,
            'form': form
        }
        return render(request, "bobotnilai/edit.html", context)

@login_required(login_url='/akun/login/')
def delete(request, id):
    bobotnilai = Bobotnilai.objects.get(pk=id)
    if bobotnilai.delete():
        messages.success(request, "Berhasil menghapus bobot nilai.")
    else:
        messages.danger(request, "Gagal menghapus bobot nilai.")
    return redirect(reverse('bobotnilai'))