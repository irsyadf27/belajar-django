from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Bobotnilai(models.Model):
    bobot = models.PositiveIntegerField()
    indeks = models.CharField(max_length=3)
    
    def __unicode__(self):
        return self.indeks