# coding: utf8
from django import forms
from django import template
from django.forms.forms import BoundField
from django.utils.safestring import mark_safe

register = template.Library()



@register.filter
def bootstrap_form_group(field, extra_class=None):
    ctx = {
        'group_class': 'error' if field.errors else '',
        'label': field.label,
        'input': bootstrap_input(field, extra_class),
        'help_text': ("<div class='help-block'>%s</div>" % field.help_text) if field.help_text else '',
        'errors': ("<p class='help-block'>%s</p>" % field.errors[0]) if field.errors else ''
    }

    if field.field.required:
        ctx['group_class'] += ' required'

    return mark_safe("""<div class='control-group %(group_class)s' >
    <label class='control-label right'>%(label)s:</label><div class='controls'>
    %(input)s %(help_text)s %(errors)s
</div></div>""" % ctx)


@register.filter
def bootstrap_input(field, extra_class=None):
    """Only show field, without label"""
    if not isinstance(field, BoundField):
        return ''

    klass = field.field.widget.attrs.get('class', '')

    klass += " form-control"
    if extra_class:
        klass += " " + extra_class

    if type(field.field.widget) == forms.Select:
        klass += ' select2'

    if field.field.required:
        field.field.widget.attrs['required'] = ''

    field.field.widget.attrs['class'] = klass
    if 'placeholder' not in field.field.widget.attrs:
        field.field.widget.attrs['placeholder'] = field.label.replace('_', ' ').title()

    if 'addon' in field.field.widget.attrs:
        return "<div class='input-group'>%s <span class='input-group-addon'>%s</span></div>" % (
            field, field.field.widget.attrs['addon'])

    return field