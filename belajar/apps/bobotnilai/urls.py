from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='bobotnilai'),
    url(r'^tambah/$', views.tambah, name='tambahbobotnilai'),
    url(r'^edit/(?P<id>\d+)', views.edit, name='editbobotnilai'),
    url(r'^delete/(?P<id>\d+)', views.delete, name='deletebobotnilai'),
]