from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^mahasiswa/$', views.list_mhs, name='mahasiswa'),
    url(r'^mahasiswa/tambah/$', views.tambah, name='tambahmahasiswa'),
    url(r'^mahasiswa/lihat/(?P<id>\d+)$', views.lihat, name='lihatmahasiswa'),
    url(r'^mahasiswa/edit/(?P<id>\d+)$', views.edit, name='editmahasiswa'),
    url(r'^mahasiswa/delete/(?P<id>\d+)', views.delete, name='deletemahasiswa'),
]