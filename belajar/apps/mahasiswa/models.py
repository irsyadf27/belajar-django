from __future__ import unicode_literals

from django.db import models
from belajar.apps.kelas.models import Kelas
from django.contrib.auth.models import User

# Create your models here.
class Mahasiswa(models.Model):
    #user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    nim = models.PositiveIntegerField()
    nama = models.CharField(max_length=50)
    jk = models.CharField(max_length=1, choices=(('L', 'Laki-Laki'), ('P', 'Perempuan')))
    tgl_lahir = models.DateField()
    alamat = models.TextField()
    foto = models.ImageField(upload_to='foto')
    kelas = models.ForeignKey(Kelas, on_delete=models.CASCADE)
    def __unicode__(self):
        return self.nama