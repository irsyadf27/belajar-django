from django import forms
from belajar.apps.mahasiswa.models import Mahasiswa
from django.contrib.auth.models import User
from belajar.apps.akun.models import Profile

class FormMahasiswa(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(FormMahasiswa, self).__init__(*args, **kwargs)
        self.fields['user'].queryset = Profile.objects.filter(jenis='Mahasiswa')
        
    class Meta:
        model = Mahasiswa
        fields = ('nim', 'nama', 'jk', 'tgl_lahir', 'alamat', 'kelas', 'foto', )
