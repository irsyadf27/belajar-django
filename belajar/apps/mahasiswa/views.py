from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings

from .models import Mahasiswa
from belajar.apps.mahasiswa.forms import FormMahasiswa


# Create your views here.
@login_required(login_url='/akun/login/')
def home(request):
    return render(request, 'mahasiswa/index.html', {'title':'CRUD Mahasiswa'})

@login_required(login_url='/akun/login/')
def list_mhs(request):
    mahasiswa = Mahasiswa.objects.all().order_by('nim')
    paginator = Paginator(mahasiswa, settings.LIMIT_PAGE)
    page = request.GET.get('page')
    
    try:
        mahasiswa = paginator.page(page)
    except PageNotAnInteger:
        mahasiswa = paginator.page(1)
    except EmptyPage:
        mahasiswa = paginator.page(paginator.num_pages)
        
    context = {'mahasiswa': mahasiswa}
    return render(request, 'mahasiswa/list.html', context)

@login_required(login_url='/akun/login/')
def tambah(request):
    form = FormMahasiswa(request.POST or None, request.FILES or None)
    if request.POST:
        if form.is_valid():
            form.save()

            messages.success(request, "Berhasil menambah mahasiswa baru.")
            return redirect(reverse('mahasiswa'))
    context = {
        'form': form
    }
    return render(request, "mahasiswa/tambah.html", context)

@login_required(login_url='/akun/login/')
def edit(request, id):
    mahasiswa = Mahasiswa.objects.get(pk=id)
    form = FormMahasiswa(request.POST or None, request.FILES or None, instance=mahasiswa)
    if request.POST:    
        if form.is_valid():
            form.save()
            
            messages.success(request, "Berhasil mengubah mahasiswa.")
            return redirect(reverse('mahasiswa'))
    else:
        context = {
            'id': id,
            'form': form
        }
        return render(request, "mahasiswa/edit.html", context)

@login_required(login_url='/akun/login/')
def delete(request, id):
    mahasiswa = Mahasiswa.objects.get(pk=id)
    if mahasiswa.delete():
        messages.success(request, "Berhasil menghapus mahasiswa.")
    else:
        messages.danger(request, "Gagal menghapus mahasiswa")
    return redirect(reverse('mahasiswa'))

@login_required(login_url='/akun/login/')
def lihat(request, id):
    pass