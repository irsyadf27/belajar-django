# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-29 05:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('kelas', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mahasiswa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nim', models.PositiveIntegerField()),
                ('nama', models.CharField(max_length=50)),
                ('jk', models.CharField(choices=[('L', 'Laki-Laki'), ('P', 'Perempuan')], max_length=1)),
                ('tgl_lahir', models.DateField()),
                ('alamat', models.TextField()),
                ('foto', models.ImageField(upload_to='foto')),
                ('kelas', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kelas.Kelas')),
            ],
        ),
    ]
